<?php
use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Ejercicio 2 Consultas';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Consultas de selección 2</h1>

       
        </div>

    <div class="body-content">

        <div class="row">
            <div class="col-sm-6 col-md-4">
                <div class ="thumbnail">
                    <div class ="caption">
                <h2>Consulta 1</h2>

                <p>Número de ciclistas que hay </p>

                <p>

<?= Html:: a('DAO', ['site/consulta1dao'], ['class' => 'btn btn-primary']) ?>
                
<?= Html::a('Active Record', ['site/consulta1orm'], ['class' => 'btn btn-default']) ?>
                </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class ="thumbnail">
                    <div class ="caption">
                <h2>Consulta 2</h2>

                <p>Número de ciclistas que hay del equipo Banesto
</p>
 <p>
                   
<?= Html:: a('DAO', ['site/consulta2dao'], ['class' => 'btn btn-primary']) ?>
     
<?= Html::a('Active Record', ['site/consulta2orm'], ['class' => 'btn btn-default']) ?>
                   
                </p>
            </div>
                    </div>
                </div>
            <div class="col-lg-4">
                <div class ="thumbnail">
                    <div class ="caption">
                <h2>Consulta 3</h2>

                <p>Edad media de los ciclistas
</p>

<?= Html:: a('DAO', ['site/consulta3dao'], ['class' => 'btn btn-primary']) ?>
                
<?= Html::a('Active Record', ['site/consulta3orm'], ['class' => 'btn btn-default']) ?>
            </div>
                     </div>
                 </div>
            </div>
        <div class="row">
            <div class="col-lg-4">
                 <div class ="thumbnail">
                    <div class ="caption">
                <h2>Consulta 4</h2>

                <p>La edad media de los del equipo Banesto</p>

<?= Html:: a('DAO', ['site/consulta4dao'], ['class' => 'btn btn-primary']) ?>
                
<?= Html::a('Active Record', ['site/consulta4orm'], ['class' => 'btn btn-default']) ?>
            </div>
                      </div>
                 </div>
            <div class="col-lg-4">
                <div class ="thumbnail">
                    <div class ="caption">
                <h2>Consulta 5</h2>
                
                 <p>La edad media de los ciclistas por cada equipo</p>

<?= Html:: a('DAO', ['site/consulta5dao'], ['class' => 'btn btn-primary']) ?>
                 
<?= Html::a('Active Record', ['site/consulta5orm'], ['class' => 'btn btn-default']) ?>
                
            </div>
                    </div>
                </div>
            <div class="col-lg-4">
                 <div class ="thumbnail">
                    <div class ="caption">
                <h2>Consulta 6</h2>

                <p>El número de ciclistas por equipo</p>

<?= Html:: a('DAO', ['site/consulta6dao'], ['class' => 'btn btn-primary']) ?>
                
<?= Html::a('Active Record', ['site/consulta6orm'], ['class' => 'btn btn-default']) ?>
            </div>
                      </div>
                 </div>
            </div>
        <div class="row">
            <div class="col-lg-4">
                <div class ="thumbnail">
                    <div class ="caption">
                <h2>Consulta 7</h2>

                <p>El número total de puertos </p>

<?= Html:: a('DAO', ['site/consulta7dao'], ['class' => 'btn btn-primary']) ?>
                
<?= Html::a('Active Record', ['site/consulta7orm'], ['class' => 'btn btn-default']) ?>
            </div>
                     </div>
                 </div>
            <div class="col-lg-4">
                <div class ="thumbnail">
                    <div class ="caption">
                <h2>Consulta 8</h2>

                <p>El número total de puertos mayores de 1500</p>

<?= Html:: a('DAO', ['site/consulta8dao'], ['class' => 'btn btn-primary']) ?>
                
<?= Html::a('Active Record', ['site/consulta8orm'], ['class' => 'btn btn-default']) ?>
            </div>
                     </div>
                 </div>
            <div class="col-lg-4">
                 <div class ="thumbnail">
                    <div class ="caption">
                <h2>Consulta 9</h2>

                <p>Listar el nombre de los equipos que tengan más de 4 ciclistas </p>

<?= Html:: a('DAO', ['site/consulta9dao'], ['class' => 'btn btn-primary']) ?>
                
<?= Html::a('Active Record', ['site/consulta9orm'], ['class' => 'btn btn-default']) ?>
            </div>
                     </div>
            </div>
             </div>
        <div class="row">
            <div class="col-lg-4">
                <div class ="thumbnail">
                    <div class ="caption">
                <h2>Consulta 10</h2>

                <p>Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32</p>

<?= Html:: a('DAO', ['site/consulta10dao'], ['class' => 'btn btn-primary']) ?>
                
<?= Html::a('Active Record', ['site/consulta10orm'], ['class' => 'btn btn-default']) ?>
            </div>
                     </div>
                 </div>
            <div class="col-lg-4">
                <div class ="thumbnail">
                    <div class ="caption">
                <h2>Consulta 11</h2>

                <p>Indícame el número de etapas que ha ganado cada uno de los ciclistas</p>

                <?= Html:: a('DAO', ['site/consulta11dao'], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Active Record', ['site/consulta11orm'], ['class' => 'btn btn-default']) ?>
            </div>
                       </div>
                   </div>
            <div class="col-lg-4">
                <div class ="thumbnail">
                    <div class ="caption">
                <h2>Consulta 12</h2>

                <p>Indícame el dorsal de los ciclistas que hayan ganado más de una etapa</p>

                <?= Html:: a('DAO', ['site/consulta12dao'], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Active Record', ['site/consulta12orm'], ['class' => 'btn btn-default']) ?>
            </div>
                       </div>
                   </div>
            
        </div>

    </div>
</div>
