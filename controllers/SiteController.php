<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
    
    public function actionConsulta1dao(){
   
    
    $dataProvider = new SqlDataProvider([
        'sql'=>' SELECT  COUNT(*)AS Numero_Ciclistas FROM ciclista',
       
        'pagination'=>[
            'pageSize' => 5,
        ]  
    ]) ;
    
    return $this->render ("resultado", [
        "resultados" =>$dataProvider,
        "campos"=>['Numero_Ciclistas'],
        "titulo"=>"Consulta 1 con DAO",
        "enunciado"=>"Número de ciclistas que hay",
        "sql" =>"SELECT COUNT(*) AS Numero_Ciclistas FROM ciclista",
    ]);
        
    }
    
    public function actionConsulta1orm(){
        $dataProvider = new ActiveDataProvider([
        'query' => \app\models\Ciclista::find()->select ("count(*) as Numero_Ciclistas"),
       'pagination' =>[
           'pageSize' =>5,
       ]        
        ]);
        
        return $this->render("resultado", [
        "resultados"=>$dataProvider,
            "campos"=>['Numero_Ciclistas'],
            "titulo"=>"Consulta 1 con Active Record",
            "enunciado"=>"Número de ciclistas que hay ",
            "sql"=>"SELECT  COUNT(*) as Numero_Ciclistas FROM ciclista",
            
        ]); 
    }
    
     public function actionConsulta2dao(){
    
    
    $dataProvider = new SqlDataProvider([
        'sql'=>' select count(*) AS ciclistas_banesto from ciclista where nomequipo = "Banesto"',
       
        'pagination'=>[
            'pageSize' => 5,
        ]  
    ]) ;
    
    return $this->render ("resultado", [
        "resultados" =>$dataProvider,
        "campos"=>['ciclistas_banesto'],
        "titulo"=>"Consulta 2 con DAO",
        "enunciado"=>"Número de ciclistas que hay del equipo Banesto ",
        "sql" =>"SELECT COUNT(*) AS ciclistas_banesto FROM ciclista where nomequipo ='Banesto' ",
    ]);
        
    }
    
    public function actionConsulta2orm(){
        $dataProvider = new ActiveDataProvider([
        'query' => \app\models\Ciclista::find()->select ("count(*) as ciclistas_banesto")->where  ("nomequipo = 'banesto'"),
       'pagination' =>[
           'pageSize' =>5,
       ]        
        ]);
        
        return $this->render("resultado", [
        "resultados"=>$dataProvider,
            "campos"=>['ciclistas_banesto'],
            "titulo"=>"Consulta 2 con Active Record",
            "enunciado"=>"Número de ciclistas que hay del equipo Banesto ",
        "sql" =>"SELECT COUNT(*) AS ciclistas_banesto FROM ciclista where nomequipo ='Banesto' ",
            
        ]); 
    }
    
     public function actionConsulta3dao(){
   
    
    $dataProvider = new SqlDataProvider([
        'sql'=>' select avg(edad) AS Edad_Media from ciclista',
        
        'pagination'=>[
            'pageSize' => 5,
        ]  
    ]) ;
    
    return $this->render ("resultado", [
        "resultados" =>$dataProvider,
        "campos"=>['Edad_Media'],
        "titulo"=>"Consulta 3 con DAO",
        "enunciado"=>"Edad media de los ciclistas",
        "sql" =>"select avg(edad) AS Edad_Media from ciclista",
    ]);
        
    }
    
    public function actionConsulta3orm(){
        $dataProvider = new ActiveDataProvider([
        'query' => \app\models\Ciclista::find()->select ("avg(edad) as Edad_Media"),
       'pagination' =>[
           'pageSize' =>5,
       ]        
        ]);
        
        return $this->render("resultado", [
        "resultados"=>$dataProvider,
            "campos"=>['Edad_Media'],
            "titulo"=>"Consulta 3 con Active Record",
            "enunciado"=>"Edad media de los ciclistas ",
            "sql"=>"select avg(edad) AS Edad_Media from ciclista",
            
        ]); 
    }
    
     public function actionConsulta4dao(){
    
    
    $dataProvider = new SqlDataProvider([
        'sql'=>' SELECT  AVG(edad)AS Edad_Media_Banesto FROM ciclista where nomequipo = "Banesto"',
        
        'pagination'=>[
            'pageSize' => 5,
        ]  
    ]) ;
    
    return $this->render ("resultado", [
        "resultados" =>$dataProvider,
        "campos"=>['Edad_Media_Banesto'],
        "titulo"=>"Consulta 4 con DAO",
        "enunciado"=>"La edad media de los del equipo Banesto",
        "sql" =>"SELECT  AVG(edad)AS Edad_Media_Banesto FROM ciclista where nomequipo = 'Banesto'",
    ]);
        
    }
    
    public function actionConsulta4orm(){
        $dataProvider = new ActiveDataProvider([
        'query' => \app\models\Ciclista::find()->select ("avg(edad) as Edad_Media_Banesto")-> where ("nomequipo = 'banesto'"),
       'pagination' =>[
           'pageSize' =>5,
       ]        
        ]);
        
        return $this->render("resultado", [
        "resultados"=>$dataProvider,
            "campos"=>['Edad_Media_Banesto'],
            "titulo"=>"Consulta 4 con Active Record",
            "enunciado"=>"La edad media de los del equipo Banesto ",
            "sql"=>"SELECT  AVG(edad)AS Edad_Media_Banesto FROM ciclista where nomequipo = 'Banesto'",
            
        ]); 
    }
    
     public function actionConsulta5dao(){
    
    
    $dataProvider = new SqlDataProvider([
        'sql'=>' SELECT  AVG(edad)AS Edad_Media_por_equipo FROM ciclista GROUP BY  nomequipo',
        
        'pagination'=>[
            'pageSize' => 5,
        ]  
    ]) ;
    
    return $this->render ("resultado", [
        "resultados" =>$dataProvider,
        "campos"=>['Edad_Media_por_equipo'],
        "titulo"=>"Consulta 5 con DAO",
        "enunciado"=>"La edad media de los ciclistas por cada equipo",
        "sql" =>"SELECT  AVG(edad)AS Edad_Media_por_equipo FROM ciclista GROUP BY  nomequipo",
    ]);
        
    }
    
    public function actionConsulta5orm(){
        $dataProvider = new ActiveDataProvider([
        'query' => \app\models\Ciclista::find()->select ("avg(edad) as Edad_Media_Por_Equipo")->groupBy('nomequipo'),
       'pagination' =>[
           'pageSize' =>5,
       ]        
        ]);
        
        return $this->render("resultado", [
        "resultados"=>$dataProvider,
            "campos"=>['Edad_Media_Por_Equipo'],
            "titulo"=>"Consulta 5 con Active Record",
            "enunciado"=>"La edad media de los ciclistas por cada equipo ",
            "sql"=>"SELECT  avg(edad) as Edad_Media_Por_Equipo FROM ciclista",
            
        ]); 
    }
    
     public function actionConsulta6dao(){
    
    
    $dataProvider = new SqlDataProvider([
        'sql'=>' select count(dorsal) AS Total_Ciclistas_equipo from ciclista group by nomequipo',
        
        'pagination'=>[
            'pageSize' => 5,
        ]  
    ]) ;
    
    return $this->render ("resultado", [
        "resultados" =>$dataProvider,
        "campos"=>['Total_Ciclistas_equipo'],
        "titulo"=>"Consulta 6 con DAO",
        "enunciado"=>"El número de ciclistas por equipo",
        "sql" =>"Select count(dorsal) AS Total_Ciclistas_equipo from ciclista group by nomequipo",
    ]);
        
    }
    
    public function actionConsulta6orm(){
        $dataProvider = new ActiveDataProvider([
        'query' => \app\models\Ciclista::find()->select ("count(dorsal) as Total_Ciclistas_equipo")->groupBy('nomequipo'),
       'pagination' =>[
           'pageSize' =>5,
       ]        
        ]);
        
        return $this->render("resultado", [
        "resultados"=>$dataProvider,
            "campos"=>['Total_Ciclistas_equipo'],
            "titulo"=>"Consulta 6 con Active Record",
            "enunciado"=>"El número de ciclistas por equipo",
            "sql"=>"Select count(dorsal) AS Total_Ciclistas_equipo from ciclista group by nomequipo",
            
        ]); 
    }
    
     public function actionConsulta7dao(){
   
    
    $dataProvider = new SqlDataProvider([
        'sql'=>' SELECT COUNT(nompuerto) AS numero_total_puertos FROM puerto',
        
        'pagination'=>[
            'pageSize' => 5,
        ]  
    ]) ;
    
    return $this->render ("resultado", [
        "resultados" =>$dataProvider,
        "campos"=>['numero_total_puertos'],
        "titulo"=>"Consulta 7 con DAO",
        "enunciado"=>"El número total de puertos",
        "sql" =>"SELECT COUNT(nompuerto) AS numero_total_puertos FROM puerto",
    ]);
        
    }
    
    public function actionConsulta7orm(){
        $dataProvider = new ActiveDataProvider([
        'query' => \app\models\Puerto::find()->select ("count(nompuerto) as numero_total_puertos"),
       'pagination' =>[
           'pageSize' =>5,
       ]        
        ]);
        
        return $this->render("resultado", [
        "resultados"=>$dataProvider,
            "campos"=>['numero_total_puertos'],
            "titulo"=>"Consulta 7 con Active Record",
            "enunciado"=>"El número total de puertos ",
            "sql"=>"SELECT COUNT(nompuerto) AS numero_total_puertos FROM puerto",
            
        ]); 
    }
    
     public function actionConsulta8dao(){
    
    
    $dataProvider = new SqlDataProvider([
        'sql'=>' SELECT  COUNT(*)AS numero_total_puertos FROM puerto where altura > 1500',
       
        'pagination'=>[
            'pageSize' => 5,
        ]  
    ]) ;
    
    return $this->render ("resultado", [
        "resultados" =>$dataProvider,
        "campos"=>['numero_total_puertos'],
        "titulo"=>"Consulta 8 con DAO",
        "enunciado"=>"El número total de puertos mayores de 1500",
        "sql" =>"SELECT COUNT(*) AS Numero_Ciclistas FROM ciclista Where altura > 1500",
    ]);
        
    }
    
    public function actionConsulta8orm(){
        $dataProvider = new ActiveDataProvider([
        'query' => \app\models\Puerto::find()->select ("count(*) as numero_total_puertos")-> where ('altura >"1500"'),
       'pagination' =>[
           'pageSize' =>5,
       ]        
        ]);
        
        return $this->render("resultado", [
        "resultados"=>$dataProvider,
            "campos"=>['numero_total_puertos'],
            "titulo"=>"Consulta 8 con Active Record",
            "enunciado"=>"El número total de puertos mayores de 1500",
            "sql"=>"SELECT  COUNT(*) as Numero_Ciclistas FROM ciclista where altura > 1500",
            
        ]); 
    }
    
     public function actionConsulta9dao(){
    
    
    $dataProvider = new SqlDataProvider([
        'sql'=>' SELECT nomequipo FROM ciclista GROUP BY nomequipo HAVING count(dorsal)>4',
        
        'pagination'=>[
            'pageSize' => 5,
        ]  
    ]) ;
    
    return $this->render ("resultado", [
        "resultados" =>$dataProvider,
        "campos"=>['nomequipo'],
        "titulo"=>"Consulta 9 con DAO",
        "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas",
        "sql" =>"SELECT nomequipo FROM ciclista GROUP BY nomequipo HAVING count(dorsal)>4",
    ]);
        
    }
    
    public function actionConsulta9orm(){
        $dataProvider = new ActiveDataProvider([
        'query' => \app\models\Ciclista::find()->select ("nomequipo") ->groupBy("nomequipo")->having("count('dorsal')>4"),
       'pagination' =>[
           'pageSize' =>5,
       ]        
        ]);
        
        return $this->render("resultado", [
        "resultados"=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=>"Consulta 9 con Active Record",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas",
            "sql"=>"SELECT nomequipo FROM ciclista GROUP BY nomequipo HAVING count(dorsal)>4",
            
        ]); 
    }
    
     public function actionConsulta10dao(){
    
    
    $dataProvider = new SqlDataProvider([
        'sql'=>' SELECT nomequipo FROM ciclista WHERE edad BETWEEN 28 AND 32 GROUP BY nomequipo HAVING count(dorsal)>4 ',
       
        'pagination'=>[
            'pageSize' => 5,
        ]  
    ]) ;
    
    return $this->render ("resultado", [
        "resultados" =>$dataProvider,
        "campos"=>['nomequipo'],
        "titulo"=>"Consulta 10 con DAO",
        "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32",
        "sql" =>"SELECT nomequipo FROM ciclista WHERE edad BETWEEN 28 AND 32 GROUP BY nomequipo HAVING count(dorsal)>4",
    ]);
        
    }
    
    public function actionConsulta10orm(){
        $dataProvider = new ActiveDataProvider([
        'query' => \app\models\Ciclista::find()->select ("nomequipo")->where ("edad between 28 and 32") ->groupBy("nomequipo")->having("count('dorsal')>4"),
       'pagination' =>[
           'pageSize' =>5,
       ]        
        ]);
        
        return $this->render("resultado", [
        "resultados"=>$dataProvider,
            "campos"=>['nomequipo'],
               "titulo"=>"Consulta 10 con DAO",
        "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32",
        "sql" =>"SELECT nomequipo FROM ciclista WHERE edad BETWEEN 28 AND 32 GROUP BY nomequipo HAVING count(dorsal)>4",
            
        ]); 
    }
    
     public function actionConsulta11dao(){
    
    
    $dataProvider = new SqlDataProvider([
        'sql'=>' SELECT dorsal AS dorsal_ciclista, count(*) AS numero_victorias FROM etapa GROUP BY dorsal',
        
        'pagination'=>[
            'pageSize' => 5,
        ]  
    ]) ;
    
    return $this->render ("resultado", [
        "resultados" =>$dataProvider,
        "campos"=>['dorsal_ciclista', 'numero_victorias'],
        "titulo"=>"Consulta 11 con DAO",
        "enunciado"=>"Indícame el número de etapas que ha ganado cada uno de los ciclistas",
        "sql" =>"SELECT dorsal AS dorsal_ciclista, count(*) AS numero_victorias FROM etapa GROUP BY dorsal",
    ]);
        
    }
    
    public function actionConsulta11orm(){
        $dataProvider = new ActiveDataProvider([
        'query' => \app\models\Etapa::find()->select ("dorsal as dorsal_ciclista, count(*) as numero_victorias")->groupBy("dorsal"),
       'pagination' =>[
           'pageSize' =>5,
       ]        
        ]);
        
        return $this->render("resultado", [
        "resultados"=>$dataProvider,
           "campos"=>['dorsal_ciclista','numero_victorias'],
        "titulo"=>"Consulta 11 con DAO",
        "enunciado"=>"Indícame el número de etapas que ha ganado cada uno de los ciclistas",
        "sql" =>"SELECT dorsal AS dorsal_ciclista, count(*) AS numero_victorias FROM etapa GROUP BY dorsal",
            
        ]); 
    }
    
     public function actionConsulta12dao(){
    
    
    $dataProvider = new SqlDataProvider([
        'sql'=>' SELECT dorsal AS dorsal_ciclista, count(*) AS numero_victorias FROM etapa GROUP BY dorsal HAVING COUNT(dorsal)>1',
       
        'pagination'=>[
            'pageSize' => 5,
        ]  
    ]) ;
    
    return $this->render ("resultado", [
        "resultados" =>$dataProvider,
        "campos"=>['dorsal_ciclista','numero_victorias'],
        "titulo"=>"Consulta 12 con DAO",
        "enunciado"=>"Indícame el dorsal de los ciclistas que hayan ganado más de una etapa",
        "sql" =>"SELECT dorsal AS dorsal_ciclista, count(*) AS numero_victorias FROM etapa GROUP BY dorsal HAVING COUNT(dorsal)>1",
    ]);
        
    }
    
    public function actionConsulta12orm(){
        $dataProvider = new ActiveDataProvider([
  'query' => \app\models\Etapa::find()->select ("dorsal as dorsal_ciclista, count(*) as numero_victorias")->groupBy("dorsal")->having("count('dorsal')>1"),
       'pagination' =>[
           'pageSize' =>5,
       ]        
        ]);
        
        return $this->render("resultado", [
        "resultados"=>$dataProvider,
            "campos"=>['dorsal_ciclista','numero_victorias'],
             "titulo"=>"Consulta 12 con DAO",
        "enunciado"=>"Indícame el dorsal de los ciclistas que hayan ganado más de una etapa",
        "sql" =>"SELECT dorsal AS dorsal_ciclista, count(*) AS numero_victorias FROM etapa GROUP BY dorsal HAVING COUNT(dorsal)>1",
            
        ]); 
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
